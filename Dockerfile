FROM python:3.11.3

ARG ARTIFACTORY_USER=${ARTIFACTORY_USER:-""}
ARG ARTIFACTORY_PW=${ARTIFACTORY_PW:-""}

RUN apt update && apt install -y curl
COPY . /code/
WORKDIR /code/

RUN pip3 install --upgrade pip
RUN pip3 install -r requirements.txt

EXPOSE 5000
CMD [ "python3", "-m" , "flask", "run", "--host=0.0.0.0"]
