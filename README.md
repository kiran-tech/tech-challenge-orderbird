# Tech-challenge-webapp-jovian
This Website is a careers website for Jovian. 

# Demo
Deploying a simple Dockerized Flask api application Python 3.9+ based on standard Python type hints.

## Prerequisites:

1. Minimum Python 3.8 
1. Docker Installed in your system
2. Account in Jfrog to pull and push docker images.

### Let's get started
## Run application on local Machine 

Follow these setup steps to install the application locally. For dependency management `pip3` is used.
1. Clone the repository.
2. Make sure that you are on the `main` branch to get the latest verified changes.
3. Make sure to be in `/tech-challenge-orderbird` path.
4. Run `pip3 install virtualenv`
5. Create virtual environment with `python -m venv env`
6. Activate virtual environment `source env/bin/activate`
7. Install dependencies with `pip3 install -r requirements.txt`
8. To check all the packages installed `pip freeze`
8. Run `flask run`  
9. Go to '`http://127.0.0.1:5000/`' on your browser


## Run Dockerized application on local Machine 
# pre-requisistes:

- Yourjfrogrepo: techchallenge.jfrog.io/docker
- Jfrog login: Username/password as variable added in gitlabci

Using 'Dockerfile'
- Run `docker build -t Yourjfrogrepo/imagename:tagname .` -> To build docker image
  e.g: docker build -t techchallenge.jfrog.io/docker/webapp:1.0.0 .
- Run `docker images` -> To check docker image 
- Run `docker push Yourjfrogrepo/imagename:tagname` -> Push image to jfrog
- Run `docker run -d -p 5000:5000 --name container_name Yourjfrogrepo/imagename:tagname` -> port forward to run app on local
- Run `docker ps` -> To check running container
- Go to '`http://127.0.0.1:5000/`' on your browser
- Run `docker exec -it conatiner_name /bin/bash` -> To enter inside container 
## Deleting resources
# Deleting container
Run `docker stop container_name`
Run `docker rm conatiner_name OR docker container prune` (It will remove all dangling containers)
# Deleting Image
Run `docker rmi Yourjfrogrepo/imagename:tagname`

## Run application with gitlabci
# Prerequisites:
1. Gitlab account 
2. register 'shell runner' for pipelines

